import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import DetailsPage from './pages/DetailsPage';
import App from './App';
import PCTPage from './pages/PCTPage';
import GSPage from './pages/GSPage';
import PAOTPage from './pages/PAOTPage';
import GalleryPage from './pages/GalleryPage';
import AboutUsPage from './pages/AboutUsPage';
import MPTNewZealand from './pages/MPTNewZealand';
import MPTAustralia from './pages/MPTAustralia';

interface state {
    locationName: string | null;
}

export default class Switcher extends Component <{}, state> {
    state = {
        locationName: null
    }
    componentDidMount() {
        fetch('https://geo.ipify.org/api/v1?apiKey=at_uHnDHGes8guVg3af0YssPnStTGwcZ').then((res) => {
            return res.json();
        }).then((data) => {            
            this.setState({ 
                locationName: data.location.country === "IL" ? "he" : "en"
            })
        });

    }
    render() {     
        const loc:string | null = this.state.locationName;
        
        if (loc !== null) {
            return (
                <Switch>
                    <Route exact path="/">
                        <Redirect to={loc} />
                    </Route>
                    <Route exact path="/:lang/">
                        <App locationName={loc} />
                    </Route>
                    <Route exact path="/:lang/contact">
                        <DetailsPage />
                    </Route>
                    <Route exact path="/:lang/personal_customized_tours">
                        <PCTPage />
                    </Route>
                    <Route exact path="/:lang/ground_services">
                        <GSPage />
                    </Route>
                    <Route exact path="/:lang/private_and_organizated_tours">
                        <PAOTPage />
                    </Route>
                    <Route exact path="/:lang/gallery">
                        <GalleryPage />
                    </Route>
                    <Route exact path="/:lang/reviews">
                        <AboutUsPage />
                    </Route>
                    <Route exact path="/:lang/most_popular/new_zealand">
                        <MPTNewZealand />
                    </Route>
                    <Route exact path="/:lang/most_popular/australia">
                        <MPTAustralia />
                    </Route>
                    <Route path="*">
                        <div>404 NOT FOUND</div>
                    </Route>
                </Switch>
            )
        } else {
            return (<div></div>)
        }
        
    }
}
