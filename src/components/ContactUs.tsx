import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class ContactUs extends Component {
    render() {
        return (
            <div className="contact-us">
                <div className="icon"></div>
                <p>
                Interested in an offer? Leave your request and we will contact you.
                </p>
                <Link to="/" className="btn-contact">contact us</Link>
                <div className="landscape"></div>
            </div>
        )
    }
}
