import React, { Component } from 'react';

export default class ToursPageFooter extends Component {
    render() {
        return (
            <footer>
                <div className="ss-links">
                    <a className="instagram" href="https://www.instagram.com/blue_mountains_tours__/"></a>
                    <a className="linkedin" href="https://www.linkedin.com/in/menashe-salomon-a262015b/?challengeId=AQG-W2BBTc6jiAAAAXMfIBgsanfx3DE5t5e8TtcyK7wcBz_2pKs1zEUlP60ysC28I0YDKkopRbTbRwqxFYv0HSH3VrlBqw-quA&submissionId=0b2d1266-afdd-1e16-a263-3583750c4d33"></a>
                    <a className="facebook" href="https://www.facebook.com/zanzatours/"></a>
                </div>
                <p>© 2020 zanzatours.com</p>
            </footer>
        )
    }
}
