import React, { Component } from 'react';
import TourPropertyCard, { ITourPropertyCard } from '../TourPropertyCard';

interface props {
    properties: ITourPropertyCard[];
}

export default class TourPropertyCardContainer extends Component<props, {}> {
    _renderCards() {
        return this.props.properties.map((prop:ITourPropertyCard, ind:number) => {
            return (
                <TourPropertyCard 
                    key={ind}
                    {...prop}
                />
            )
        })
    }
    render() {
        return (
            <div className="tour-property-card-container">
                {this._renderCards.bind(this)()}
            </div>
        )
    }
}
