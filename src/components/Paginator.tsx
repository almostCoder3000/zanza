import React, { Component } from 'react';

interface state {
    currentIndex: number;
    winWidth: number;
    winHeight: number;
}
interface props {
    count: number;
    switch: (indexEl:number) => void;
    isTourCards: boolean;
}

export default class Paginator extends Component<props, state> {
    state = {
        currentIndex: 0,
        winWidth: 0,
        winHeight: 0
    }
    
    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }
    
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
    
    updateWindowDimensions = () => {
        this.setState({ winWidth: window.innerWidth, winHeight: window.innerHeight });
    }
    _renderCircles() {
        var circles:any[] = [];
        for (var _i = 0; _i < this.props.count; _i++) {
            circles.push(
                <span key={_i} className={this.state.currentIndex === _i ? 'active' : ""}></span>
            )
        }
        return circles;
    }

    _onPressSwitch (step: number) {
        let newInd:number = this.state.currentIndex + step;
        if (newInd >= 0 && newInd < this.props.count) {
            this.setState({ 
                currentIndex: newInd
            });
            this.props.switch(newInd);
        }
    }

    render() {
        if (this.props.count <= 3 && this.state.winWidth >= 1260 && this.props.isTourCards) {
            return (<span></span>)
        }      
        return (
            <div className="paginator">
                <button 
                    className="left" 
                    onClick={this._onPressSwitch.bind(this, -1)}
                ></button>
                <div className="circles">
                    {this._renderCircles.bind(this)()}
                </div>
                <button 
                    className="right" 
                    onClick={this._onPressSwitch.bind(this, 1)}
                ></button>
            </div>
        )
    }
}
