import React, { Component, ComponentState } from 'react';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';
import ILocalization from '../localization/interface';
import he from '../localization/he';
import en from '../localization/en';
import ReactGA from 'react-ga';

ReactGA.initialize('UA-161289106-1');

interface IAnswer {
    title: string;
    next: number;
    type: "button" | "text" | "checkbox";
}

interface IQuestion {
    title: string;
    answers: IAnswer[];
}

const questions_hebrew:IQuestion[] = [
	{ 
		title: "?האם אתם מתכננים לנסוע", 
		answers: [
			{ title: "כן", next: 1, type: "button" },
			{ title: "לא", next: -1, type: "button" }
		] 
	},
	{ 
		title: "?מתי אתה מתכנן לנסוע", 
		answers: [
			{ title: "החודש", next: 2, type: "button" },
			{ title: "חודש הבא", next: 2, type: "button" },
			{ title: "השנה", next: 2, type: "button" },
			{ title: "אני עדיין לא יודע", next: 2, type: "button" }
		] 
	},
	{ 
		title: "?מאיפה אתה", 
		answers: [
			{ title: "אוסטרליה / ניו זילנד", next: 3, type: "button" },
			{ title: 'ארה"ב', next: 3, type: "button" },
			{ title: "אירופה", next: 3, type: "button" },
			{ title: "אחר", next: 3, type: "text" }
		]
	},
	{ 
		title: "?כמה זמן אתם נוסעים", 
		answers: [
			{ title: "שבוע", next: 4, type: "button" },
			{ title: "חודש", next: 4, type: "button" },
			{ title: "1 + חודש", next: 4, type: "button" },
			{ title: "אני עדיין לא יודע", next: 4, type: "button" }
		] 
	},
	{ 
		title: "?באיזה טיול אתה מתעניין", 
		answers: [
			{ title: "גילוי", next: 5, type: "checkbox" },
			{ title: "תרבות", next: 5, type: "checkbox" },
			{ title: "משפחה", next: 5, type: "checkbox" },
            { title: "אחר", next: 5, type: "text" },
            { title: "לְהַמשִׁיך", next: 5, type: "button" }
		]
	}
]

const questions:IQuestion[] = [
	{ 
		title: "Are you planning to travel?", 
		answers: [
			{ title: "yes", next: 1, type: "button" },
			{ title: "no", next: -1, type: "button" }
		] 
	},
	{ 
		title: "When are you planning to travel?", 
		answers: [
			{ title: "This month", next: 2, type: "button" },
			{ title: "Next month", next: 2, type: "button" },
			{ title: "This year", next: 2, type: "button" },
			{ title: "I don’t know yet", next: 2, type: "button" }
		] 
	},
	{ 
		title: "Where are you from?", 
		answers: [
			{ title: "Australia/NZ", next: 3, type: "button" },
			{ title: "USA", next: 3, type: "button" },
			{ title: "Europe", next: 3, type: "button" },
			{ title: "Other", next: 3, type: "text" }
		]
	},
	{ 
		title: "How long are you traveling for?", 
		answers: [
			{ title: "Week", next: 4, type: "button" },
			{ title: "Month", next: 4, type: "button" },
			{ title: "1 + month", next: 4, type: "button" },
			{ title: "I don’t know yet", next: 4, type: "button" }
		] 
	},
	{ 
		title: "What kind of travel are you interested in?", 
		answers: [
			{ title: "Discovery", next: 5, type: "checkbox" },
			{ title: "Culture", next: 5, type: "checkbox" },
			{ title: "Family", next: 5, type: "checkbox" },
            { title: "Other", next: 5, type: "text" },
            { title: "Continue", next: 5, type: "button" }
		]
	}
]

interface props extends RouteComponentProps {
    saveTestResults: (results:string) => Promise<void>;
	submitEmail: (email:string, test_results:string, cb: (res:any) => void) => Promise<void>;
}

type state = {
    currentQuestion: number;
    answers: string;
    otherText: string;
    activeChoicesChbxs: string[]; 
    testState: string;//"test" | "visit_soon" | "leave_contact"
    email: string;
}

class Test extends Component<props, state> {
    state = {
        currentQuestion: 0,
        answers: "",
        otherText: "",
        activeChoicesChbxs: [],
        testState: "test",
        email: ""
    }

    _onChange(e:React.ChangeEvent<HTMLInputElement>) {
        this.setState({otherText: e.target.value});
    }

    selectChoice(choice:string) {
        let result:string[] = this.state.activeChoicesChbxs;
        if (result.indexOf(choice) === -1) {
            result = result.concat(choice);
        } else {
            result.splice(result.indexOf(choice), 1);
        }
        
        this.setState({
            activeChoicesChbxs: result
        });
    }

    _next(answer:IAnswer) {
        let answerText:string = "";
        if (this.state.currentQuestion === 4) {
            
            answerText = `${this.state.otherText} ${this.state.activeChoicesChbxs.join(" ")}`;
            this.setState({activeChoicesChbxs: []});
        } else {
            answerText = this.state.otherText === "" ? answer.title : this.state.otherText;
        }

        if (answer.next === -1) {
            this.setState({testState: "visit_soon"});
            return;
        } else if (answer.next === 5) {
            this.setState({
                testState: "leave_contact",
                answers: `${this.state.answers} | ${this.state.currentQuestion + 1}. ${answerText}`,
                otherText: ""
            });
            ReactGA.event({
                category: "Questionnaire",
                action: "Finished"
            });
            return;
        }


        this.setState({
            currentQuestion: answer.next,
            answers: `${this.state.answers} | ${this.state.currentQuestion + 1}. ${answerText}`,
            otherText: ""
        }, () => {console.log(this.state.answers)});
    }

    onChange(e:React.ChangeEvent<HTMLInputElement>) {
        this.setState({[e.target.name] : e.target.value} as ComponentState);   
    }

    _handleFormResults(res:any) {
        if (res.success) {
            alert("Thank you! We'll contact you soon...");
            window.location.href = "/";
        }
    }

    submitForm() {
        let isValid:boolean = true;
        
        ReactGA.event({
            category: "Submit contact",
            action: "Valid submition"
        });
        this.props.submitEmail(
            this.state.email,
            this.state.answers,
            this._handleFormResults.bind(this)
        );
    }

    _renderAnswers(answers:IAnswer[]) {
        const lang:any = this.props.match.params;
		const locationObj:ILocalization = lang.lang === "he" ? he : en;	
        return answers.map((answer:IAnswer, ind:number) => {
            switch(answer.type) {
                case "button":
                    return (
                        <button 
                            onClick={this._next.bind(this, answer)}
                            key={ind}
                        >
                            {answer.title}
                        </button>
                    )

                case "text":
                    return (
                        <div key={ind} 
                            className="other-choice"
                        >
                            <input type="text" placeholder={locationObj.mainPage.test.placeholders.other} onChange={this._onChange.bind(this)} />
                            <button onClick={this._next.bind(this, answer)}></button>
                        </div>
                    )

                case "checkbox":
                    let choices:string[] = this.state.activeChoicesChbxs;
                    return (
                        <div 
                            key={ind} 
                            className="checkbox-choice"
                            onClick={this.selectChoice.bind(this, answer.title)}
                        >
                            <div 
                                className={`choice ${choices.indexOf(answer.title) === -1 ? "" : "active"}`}
                            >{answer.title}</div>
                            <div className="active-icon"></div>
                        </div>
                    );

                default:
                    break;
            }
        })
    }

    saveTest() {
        this.props.saveTestResults(this.state.answers);
    }

    render() {
        const lang:any = this.props.match.params;
        let question = lang.lang === "he" ? 
            questions_hebrew[this.state.currentQuestion] : 
            questions[this.state.currentQuestion];

        let content;
		const locationObj:ILocalization = lang.lang === "he" ? he : en;	

        switch (this.state.testState) {
            case "test":
                content = (
                    <>
                        <h2>{question.title}</h2>
                        <div className="answers">
                            {this._renderAnswers.bind(this)(question.answers)}
                        </div>
                        <div className="pagination-circles">
                            <span className="active"></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </>
                )
                break;

            case "visit_soon":
                content = (
                    <>
                        <h2>{locationObj.mainPage.test.visit_soon.title}</h2>
                        <Link to={`/${lang.lang}/contact`}>{locationObj.mainPage.test.visit_soon.link}</Link>
                    </>
                )
                break;

            case "leave_contact":
                content = (
                    <>
                    <h2>{locationObj.mainPage.test.leave_contact.link}</h2>
                    <div className="form-details">
                        <div className={`input-text`}>
                            <label htmlFor="email">{locationObj.contactPage.form.emailField.label}</label>
                            <input 
                                type="text" name="email" id="email" 
                                onChange={this.onChange.bind(this)}
                                placeholder={locationObj.contactPage.form.emailField.placeholder}/>
                            <i></i>
                            <span></span>
                        </div>
                        <input 
                            type="submit" 
                            value={locationObj.contactPage.form.button}
                            onClick={this.submitForm.bind(this)}
                        />
                    </div>
                    </>
                )
                break;

            default:
                break;
        }
        return (
            <>
                <div className="img">
                    {this.state.currentQuestion + 1}
                </div>
                {content}
            </>
        )
    }
}


export default withRouter(Test);