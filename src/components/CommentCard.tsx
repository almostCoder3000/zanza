import React, { Component } from 'react';

type props = {
    title: string;
    rating: number;
    text: string;
    addressant: string;
    date: string;
    style: React.CSSProperties
}

export default class CommentCard extends Component<props> {
    _renderStars() {
        let stars:any[] = [];
        for (let _i = 0; _i < this.props.rating; _i++) {
            stars.push(
                <span key={_i} className="active"></span>
            )
        }
        for (let _i = 0; _i < 5 - this.props.rating; _i++) stars.push(<span key={_i+5}></span>);

        return stars;
    }
    render() {
        return (
            <div className="review" style={this.props.style}>
                <h2>{this.props.title}</h2>
                <div className="stars">
                    {this._renderStars.bind(this)()}
                </div>
                <p>{this.props.text}</p>
                <div className="addresser">
                    <span className="name">{this.props.addressant}</span>
                    <span className="date">{this.props.date}</span>
                </div>
            </div>
        )
    }
}
