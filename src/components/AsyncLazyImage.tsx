import React, { Component } from 'react';

interface props extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
    src: string;
    big_img_class_name: string;
}

export default class AsyncLazyImage extends Component<props, {}> {
    selfRef:React.RefObject<HTMLDivElement> = React.createRef();
    
    componentDidMount() {
        const imgTest = new Image();
        imgTest.src = this.props.src;

        imgTest.onload = (self) => {
            this.selfRef.current?.classList.add(this.props.big_img_class_name);
        };
    }

    render() {
        return (
            <div ref={this.selfRef} {...this.props}>
                {this.props.children}
            </div>
        )
    }
}
