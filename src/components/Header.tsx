import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import { HashLink as Link } from 'react-router-hash-link';
import ILocalization from '../localization/interface';
import he from '../localization/he';
import en from '../localization/en';

interface props  {
    
}

interface state {
    isActive: boolean;
    isOpened: boolean;
}

class Header extends Component<RouteComponentProps<props>, state> {
    state = {
        isActive: false,
        isOpened: false
    }

    componentDidMount() {
        window.addEventListener("scroll", this.onScroll, false);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.onScroll, false);
    }

    onScroll = () => {
        if (window.pageYOffset > 50) {
            if (!this.state.isActive) this.setState({isActive: true});
        } else {
            this.setState({isActive: false});
        }
    }

    toggleMenu() {
        this.setState({isOpened: !this.state.isOpened});
    }
    render() {
        const lang:any = this.props.match.params;
		const locationObj:ILocalization = lang.lang === "he" ? he : en;
        
        return (
            <header className={this.state.isActive ? "active" : ""}>
                <Link to={`/${lang.lang}`} className={`logo ${this.state.isOpened ? "active" : ""}`}>
                   
                </Link>
                
                <div className={`burger ${this.state.isOpened ? "close" : ""}`} onClick={this.toggleMenu.bind(this)}></div>
                <div className={`menu ${this.state.isOpened ? "active" : ""}`}>                    
                    <div 
                        className="nav"
                    >
                        <Link 
                            to={`/${lang.lang}#types_of_tours`} 
                            onClick={this.toggleMenu.bind(this)}
                        >{locationObj.mainPage.header.titles[0]}</Link>
                        <ul>
                            <li>
                                <Link to={`/${lang.lang}#personal_customized_tours`}>
                                    {locationObj.mainPage.header.our_services_dropdown[0]}
                                </Link>
                            </li>
                            <li>
                                <Link to={`/${lang.lang}#ground_services`}>
                                    {locationObj.mainPage.header.our_services_dropdown[1]}
                                </Link>
                            </li>
                            <li>
                                <Link to={`/${lang.lang}#private_and_organizated_tours`}>
                                    {locationObj.mainPage.header.our_services_dropdown[2]}
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="nav">
                        <Link 
                            to={`/${lang.lang}#most_popular`} 
                            onClick={this.toggleMenu.bind(this)}
                        >
                            {locationObj.mainPage.header.titles[1]}
                        </Link>
                        <ul>
                            <li>
                                <Link to={`/${lang.lang}/most_popular/new_zealand`}>
                                    {locationObj.mainPage.header.most_popular_dropdown[0]}
                                </Link>
                            </li>
                            <li>
                                <Link to={`/${lang.lang}/most_popular/australia`}>
                                    {locationObj.mainPage.header.most_popular_dropdown[1]}
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="nav">
                        <Link 
                            to={`/${lang.lang}#about_us`} 
                            onClick={this.toggleMenu.bind(this)}
                        >{locationObj.mainPage.header.titles[2]}</Link>
                        <ul>
                            <li>
                                <Link to={`/${lang.lang}/reviews`}>
                                    {locationObj.mainPage.header.about_us_dropdown[0]}
                                </Link>
                            </li>
                        </ul>
                    </div>
                    <div className="nav">
                        <Link 
                            to={`/${lang.lang}/contact`} 
                            onClick={this.toggleMenu.bind(this)}
                        >{locationObj.mainPage.header.titles[3]}</Link>
                    </div>
                    <div className="nav">
                        <Link 
                            to={`/${lang.lang}/gallery`} 
                            onClick={this.toggleMenu.bind(this)}
                        >{locationObj.mainPage.header.titles[4]}</Link>
                    </div>
                    <div className="nav">
                        <Link 
                            to={`/${lang.lang === 'en' ? 'he': 'en'}`} 
                            className="lang" 
                            onClick={this.toggleMenu.bind(this)}
                        >{lang.lang === 'en' ? 'he': 'en'}</Link>
                    </div>
                </div>
            </header>
        )
    }
}

export default withRouter(Header);