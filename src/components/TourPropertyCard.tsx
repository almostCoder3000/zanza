import React, { Component } from 'react';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';

interface props extends ITourPropertyCard, RouteComponentProps {}

class TourPropertyCard extends Component<props, {}> {
    render() {
        const lang:any = this.props.match.params;
        return (
            <div className={`tour-property-card ${this.props.color}`}>
                <div className={`icon ${this.props.icon_name}`}></div>
                <h3>{this.props.title}</h3>
                <p>{this.props.description}</p>
                <Link to={`/${lang.lang}/contact`} className="btn-contact">Contact us</Link>
            </div>
        )
    }
}

export default withRouter(TourPropertyCard);

export interface ITourPropertyCard {
    title: string;
    description: string;
    icon_name: string;
    color: "yellow" | "main_blue" | "green" | "dark_blue" | "red" | "pink";
}