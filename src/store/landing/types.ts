export interface IComment {
    pk: string;
    text: string;
    date: string;
    addressant: string;
    rating: number;
    title: string;
}

export interface ITour {
    pk: string;
    title: string;
    price: number;
    description: string;
    photo: string;
    title_hebrew: string;
    description_hebrew: string;
    on_click_bookatour: string | null;
}

export interface IDetails {
    isAgree: boolean;
    name: string;
    phone_number: string;
    email: string;
}

export interface IPhoto {
    src: string;
    width: number;
    height: number;
    srcSet?: string | string[];
    title?: string;
    preview: string;
}

export interface LandingState {
    fetching: boolean;
    tours: ITour[];
    test_results: string;
    comments: IComment[];
    gallery: IPhoto[];
    error: Object | undefined;
}


export const START_FETCH = "START_FETCH";
export const FETCH_ERROR = "FETCH_ERROR";

// success
export const GET_TOURS = "GET_TOURS";
export const GET_COMMENTS = "GET_COMMENTS";
export const SUBMIT_DETAILS = "SUBMIT_DETAILS";
export const SAVE_TEST_RESULTS = "SAVE_TEST_RESULTS";
export const GET_GALLERY = "GET_GALLERY";
export const SUBMIT_EMAIL = "SUBMIT_EMAIL";




export interface StartFetchAction {
    type: typeof START_FETCH;
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload?: { error: Object | undefined }
}


interface SaveTestResults {
    type: typeof SAVE_TEST_RESULTS;
    payload: string;
}

interface GetTours {
    type: typeof GET_TOURS;
    payload: ITour[];
}

interface GetComments {
    type: typeof GET_COMMENTS;
    payload: IComment[];
}

interface GetGallery {
    type: typeof GET_GALLERY;
    payload: IPhoto[];
}

interface SubmitDetails {
    type: typeof SUBMIT_DETAILS;
    payload: { success: boolean, error?: string }
}

interface SubmitEmail {
    type: typeof SUBMIT_EMAIL;
    payload: { success: boolean, error?: string }
}


export type LandingActionTypes = StartFetchAction 
                              | FetchActionError
                              | GetComments
                              | GetTours
                              | SubmitDetails
                              | SaveTestResults
                              | GetGallery
                              | SubmitEmail;
