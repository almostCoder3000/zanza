import {
    START_FETCH,
    FETCH_ERROR,
    LandingState,
    LandingActionTypes,
    GET_TOURS,
    GET_COMMENTS,
    SUBMIT_DETAILS,
    SAVE_TEST_RESULTS,
    GET_GALLERY,
  } from "./types";
  import config from "../../configure";
  
  const initialState: LandingState = {
    fetching: false,
    comments: [],
    tours: [],
    test_results: "",
    gallery: [],
    error: ""
  };
  
  export function landingReducer( 
      state = initialState, 
      action: LandingActionTypes
  ): LandingState {

      switch (action.type) {
        case START_FETCH:
            return {
                ...state,
                fetching: true
            };

        case GET_GALLERY:
            return {
                ...state,
                fetching: false,
                gallery: action.payload.map((photo) => ({
                    ...photo, 
                    src: config.urlServer + photo.src,
                    preview: config.urlServer + photo.preview
                }))
            }

        case SAVE_TEST_RESULTS:
            return {
                ...state,
                test_results: action.payload
            }

        case SUBMIT_DETAILS:
            return {
                ...state,
                fetching: false
            }

        case GET_TOURS:
            return {
                ...state,
                tours: action.payload,
                fetching: false
            }

        case GET_COMMENTS:
            return {
                ...state,
                comments: action.payload,
                fetching: false
            }

        case FETCH_ERROR:
            return {
                ...state,
                fetching: false,
                error: action.payload
            };


        default:
            return state;
      }
  }
