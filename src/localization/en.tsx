import ILocalization from "./interface";

var en:ILocalization = {
    mainPage: {
        header: {
            titles: [
                "our services", 
                "most popular tours", 
                "about us", 
                "contact us", 
                "gallery"
            ],
            our_services_dropdown: ["Personal Customized tours", "Ground services", "Private and Organized Tours"],
            about_us_dropdown: ["reviews"],
            most_popular_dropdown: ["New-Zealand", "Australia"]
        },
        aboutUs: {
            title: "about us",
            descriptionDesktop: "Zanza Tours is a small group owned by Menashe Salomon and six times award-winning since 2013. Zanza designs customized travel programs that match your specific travel requirements. At Zanza, we strive to create new and unique ideas that make each experience special and memorable. Every guest is treated like a V.I.P. by our passionate tour guide, which provides close and personal service, which guarantees to deliver better travel. That’s why all our clients come back time and time again.",
            descriptionMobile: "Zanza Tours is a small group owned by Menashe Salomon, and 6 times award winning since 2013. Zanza designs customized travel programs that match your specific travel requirements"
        },
        mostPopularTours: {
            title: "most popular tours"
        },
        typesOfTours: {
            title: "Our services",
            description: "Zanza has been pioneering experiential luxury travel since 1962. Join us and explore the world in style.",
            items: [
                { title: "Personal Customized Tour", description: "Consult with our experts on the best scenery, attractions, and experiences to fit your group. Luxurious and personal service" },
                { title: "Ground services", description: "Flights, Hotels, Cars, Attractions, workshops, and anything to make your time perfect" },
                { title: "Private and Organized Tours", description: "A variety of organized tours in several languages, as well as private tours for couples and closed groups with the most professional and experienced guides" }
            ]
        },
        test: {
            visit_soon: {
                title: "Thanks! Please visit us soon!",
                link: "Leave you contact detail for free consultation."
            },
            leave_contact: {
                title: "Thanks!",
                link: "Leave you contact detail for free consultation."
            },
            placeholders: {
                other: "other"
            }
        }
    },
    contactPage: {
        mainBlock: {
            title: "Australia and New Zealand Like Never Seen Before",
            items: [
                "Local and International flights with the best airlines at exclusive prices",
                "Hotels, Tours, Attractions, Workshops and more",
                "Shuttles to and from Airports and Tours",
                "Close and personal treatments from our best tour guides"
            ]
        },
        form: {
            description: "Custom Tours and Vacations In Australia and New Zealand. Guaranteed exclusive and unforgettable experience. For more details: ",
            title: "Leave you contact details, and we will get back to you as soon as possible",
            nameField: {
                label: "Your name",
                placeholder: "Enter your name"
            },
            phoneField: {
                label: "Phone number",
                placeholder: "+61 2(2) 123-45-67"
            },
            emailField: {
                label: "Email address",
                placeholder: "Enter email address"
            },
            agreeField: "I agree to the processing of my personal information",
            button: "start my journey"
        }
    },
    mostPopularPage: {
        nz: {
            title: "Most popular tours of New-Zealand"
        },
        au: {
            title: "Most popular tours of Australia"
        }
    }
}

export default en;