interface ITypeOfTour {
    title: string;
    description: string;
}
export default interface ILocalization {
    mainPage: {
        header: {
            titles: string[],
            our_services_dropdown: string[],
            about_us_dropdown: string[],
            most_popular_dropdown: string[];
        },
        aboutUs: {
            title: string;
            descriptionDesktop: string;
            descriptionMobile: string;
        },
        mostPopularTours: {
            title: string;
        },
        typesOfTours: {
            title: string;
            description: string;
            items: ITypeOfTour[];
        },
        test: {
            visit_soon: {
                title: string;
                link: string;
            },
            leave_contact: {
                title: string;
                link: string;
            },
            placeholders: {
                other: string;
            }
        }
    },
    contactPage: {
        mainBlock: {
            title: string,
            items: string[]
        },
        form: {
            description: string,
            title: string,
            nameField: {
                label: string,
                placeholder: string
            },
            phoneField: {
                label: string,
                placeholder: string
            },
            emailField: {
                label: string,
                placeholder: string
            },
            agreeField: string,
            button: string
        }
    },
    mostPopularPage: {
        nz: {
            title: string;
        },
        au: {
            title: string;
        }
    }
}