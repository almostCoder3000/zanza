import config from "./configure";
import axios from 'axios';

/*function getCookie(name:string):string {
    var cookieValue = "";
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}*/

async function postRequest(url:string, data:any) {
    //data.csrfmiddlewaretoken = getCookie('csrftoken');
    
    return axios({
        method: 'post',
        url: config.urlServer + url,
        data: data,
    }).then((res) => {
        return res.data;
    }).catch(err => {
        console.log('Error', err.response.status)
    });
}

async function getRequest(url:string, params:any) {
    let url_fetch = config.urlServer + url + "?";
    Object.keys(params).forEach((key:string) => url_fetch += `${key}=${params[key]}&`);
    url_fetch = url_fetch.slice(0, -1);

    return axios.get(url_fetch).then(resp => {
        console.log('Response', resp)
        return resp.data
    }).catch(err => {
        console.log('Error', err.response.status)
    });
}


export {postRequest, getRequest};
