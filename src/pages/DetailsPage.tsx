import React, { Component, ComponentState } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { submitDetails } from '../store/landing/actions';
import { AppState } from '../store';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import ILocalization from '../localization/interface';
import he from '../localization/he';
import en from '../localization/en';
import ReactGA from 'react-ga';
import AsyncLazyImage from '../components/AsyncLazyImage';
import bigBckg from '../assets/img/desktop/details-back-z2.png';

ReactGA.initialize('UA-161289106-1');


interface props extends RouteComponentProps {
    submitDetails: (
        isAgree:boolean, name:string, 
        phone_number:string, email:string, test_results:string,
        cb: (res:any) => void
    ) => Promise<void>;
    test_results: string;
}

type state = {
    isAgree: boolean;
    name: string;
    phone_number: string;
    email: string;
    errors: {
        name: boolean | null,
        phone_number: boolean | null,
        email: boolean | null,
        isAgree: boolean | null
    };
    messages: {
        name: string | null,
        phone_number: string | null,
        email: string | null,
        isAgree: string | null
    }
}

class DetailsPage extends Component<props, state> {
    state = {
        isAgree: false,
        name: "",
        phone_number: "",
        email: "",
        errors: {
            name: null,
            phone_number: null,
            email: null,
            isAgree: null
        },
        messages: {
            name: null,
            phone_number: null,
            email: null,
            isAgree: null
        }
    }

    componentDidMount() {

    }

    switchCheckbox() {
        this.setState({ isAgree: !this.state.isAgree });
    }

    onChange(e:React.ChangeEvent<HTMLInputElement>) {
        this.setState({[e.target.name] : e.target.value} as ComponentState);
        this.setState({
            errors: {
                ...this.state.errors,
                [e.target.name]: e.target.value.length === 0
            }
        });   
    }

    _handleFormResults(res:any) {
        if (res.success) {
            alert("Thank you! We'll contact you soon...");
            window.location.href = "/";
        }
    }

    submitForm() {
        let isValid:boolean = true;
        let messages = {
            name: "",
            phone_number: "",
            email: "",
            isAgree: ""
        };

        ["name", "email", "phone_number", "isAgree"].forEach((field_name) => {
            switch (field_name) {
                case "name":
                    if (this.state.name.length === 0) {
                        messages.name = "You haven't entered your name.";
                        isValid = false;
                    }
                    break;

                case "phone_number":
                    if (this.state.phone_number.length === 0) {
                        messages.phone_number ="You haven't entered your phone number.";
                        isValid = false;
                    }
                    break;

                case "email":                    
                    if (this.state.email.length === 0) {                        
                        messages.email = "You haven't entered your email.";
                        isValid = false;
                    }
                    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    if (!re.test(String(this.state.email).toLowerCase())) {
                        messages.email = "Please enter a valid email address so we can get in touch.";
                        isValid = false;
                    }
                    break;

                case "isAgree":
                    if (!this.state.isAgree) {
                        messages.isAgree = "Please provide consent to allow us to process your enquiry.";
                        isValid = false;
                    }
                    break;

                default:
                    break;
            }
        });
        this.setState({messages: messages}, () => console.log(this.state.messages));

        if (isValid) {
            ReactGA.event({
                category: "Submit contact",
                action: "Valid submition"
            });
            this.props.submitDetails(
                this.state.isAgree,
                this.state.name,
                this.state.phone_number,
                this.state.email,
                this.props.test_results,
                this._handleFormResults.bind(this)
            );
        }
        
        
    }

    validateField(name: "name" | "phone_number" | "email" | "isAgree"): string {
        let status = this.state.errors[name];

        if (status === null) {
            return "start";
        } else if (status) {
            return "error";
        } else {
            return "success";
        }
    }

    render() {
        const lang:any = this.props.match.params;
		const locationObj:ILocalization = lang.lang === "he" ? he : en;	
        
        return (
            <div className={`details-page ${lang.lang === "he" ? "he" : "en"}`}>
                <AsyncLazyImage 
                    className="details-page__main-block"
                    big_img_class_name="active"
                    src={bigBckg}
                >
                    <div className="details-page__header">
                        <Link to={`/${lang.lang}`} className="logo">
                            
                        </Link>
                        <Link to={`/${lang.lang === 'en' ? 'he': 'en'}/contact`} className="lang">
                            {lang.lang === 'en' ? 'he': 'en'}
                        </Link>
                    </div>
					<div className="details-page__description">
						<h1>{locationObj.contactPage.mainBlock.title}</h1>
						<ul>
                            {locationObj.contactPage.mainBlock.items.map((item, ind) => {
                                return (
                                    <li key={ind}>{item}</li>
                                )
                            })}
                        </ul>
					</div>
                </AsyncLazyImage>
                <div className="desktop-bottom-landscape"></div>
                <div className="form-details">
                    <p>
                        {locationObj.contactPage.form.description}
                        <br></br>
                        <a style={{color: "#ffd400"}} href="tel:+61430049139">+61 430 049139</a>
                    </p>
                    <h1>{locationObj.contactPage.form.title}</h1>
                    <div className="divider"></div>
                    <div className={`input-text ${this.validateField.bind(this)("name")}`}>
                        <label htmlFor="name">{locationObj.contactPage.form.nameField.label}</label>
                        <input 
                            type="text" name="name" id="name" 
                            placeholder={locationObj.contactPage.form.nameField.placeholder} onChange={this.onChange.bind(this)} />
                        <i></i>
                        <span>{this.state.messages.name}</span>
                    </div>
                    <div className={`input-text ${this.validateField.bind(this)("phone_number")}`}>
                        <label htmlFor="phone_number">{locationObj.contactPage.form.phoneField.label}</label>
                        <input 
                            type="text" name="phone_number" id="phone_number" 
                            placeholder="+61 2 (2) 123-45-67" onChange={this.onChange.bind(this)}/>
                        <i></i>
                        <span>{this.state.messages.phone_number}</span>
                    </div>
                    <div className={`input-text ${this.validateField.bind(this)("email")}`}>
                        <label htmlFor="email">{locationObj.contactPage.form.emailField.label}</label>
                        <input 
                            type="text" name="email" id="email" 
                            placeholder={locationObj.contactPage.form.emailField.placeholder} onChange={this.onChange.bind(this)}/>
                        <i></i>
                        <span>{this.state.messages.email}</span>
                    </div>
                    <div 
                        className={`input-checkbox ${this.state.isAgree ? "active" : ""} ${this.validateField.bind(this)("isAgree")}`} 
                        onClick={this.switchCheckbox.bind(this)}
                    >
                        <label htmlFor="isAgree">{locationObj.contactPage.form.agreeField}</label>
                        <input type="checkbox" name="isAgree" id="isAgree"/>
                        <span>{this.state.messages.isAgree}</span>
                    </div>
                    <input type="submit" value={locationObj.contactPage.form.button} onClick={this.submitForm.bind(this)}/>
                </div>
                <footer className="details-footer">
                    <div className="ss-links">
                        <a className="instagram" href="https://www.instagram.com/blue_mountains_tours__/"></a>
                        <a className="linkedin" href="https://www.linkedin.com/in/menashe-salomon-a262015b/?challengeId=AQG-W2BBTc6jiAAAAXMfIBgsanfx3DE5t5e8TtcyK7wcBz_2pKs1zEUlP60ysC28I0YDKkopRbTbRwqxFYv0HSH3VrlBqw-quA&submissionId=0b2d1266-afdd-1e16-a263-3583750c4d33"></a>
                        <a className="facebook" href="https://www.facebook.com/zanzatours/"></a>
                    </div>
                    <span>© 2020 zanzatours.com</span> 
                </footer>
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
	test_results: state.landing.test_results
});


const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	submitDetails: async (
            isAgree:boolean, name:string, phone_number:string, 
            email:string, test_results:string, cb: (res:any) => void
    ) => {
		dispatch(
            submitDetails(
                isAgree, name, phone_number, 
                email, test_results, cb
            )
        );
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(DetailsPage));