import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import MainBlockForToursPage from '../components/MainBlockForToursPage';
import Header from '../components/Header';
import { ITourPropertyCard } from '../components/TourPropertyCard';
import TourPropertyCardContainer from '../components/containers/TourPropertyCardContainer';
import ContactUs from '../components/ContactUs';
import ToursPageFooter from '../components/ToursPageFooter';

interface props extends RouteComponentProps {

}

class PAOTPage extends Component<props, {}> {
    render() {
		const lang:any = this.props.match.params;
        return (
            <div className={`tours-page ${lang.lang === "he" ? "he" : "en"}`}>
                <Header />
                <MainBlockForToursPage
                    title="Private and Organized Tours"
                    description={`Pick and choose from some of the 
                    most popular tours. Can fit small private groups 
                    and large organized tours`}
                />
                <div className="content paot-page">
                    <TourPropertyCardContainer
                        properties={properties}
                    />
                    <div className="line first"></div>
                    <div className="line second"></div>
                    <div className="line third"></div>
                    <div className="shape first"></div>
                    <div className="shape second"></div>
                </div>
                <ContactUs />
                <ToursPageFooter />
            </div>
        )
    }
}

const properties:ITourPropertyCard[] = [
    {title: "Melbourne and South Victoria", icon_name: "melbourne_and_south_victoria", color: "main_blue", description: ""},
    {title: "Between Brisbane and Sydney", icon_name: "between_brisbane_and_sydney", color: "yellow", description: ""},
    {title: "Tropical North - darwin to cairns", icon_name: "tropical_north", color: "green", description: ""},
]


export default withRouter(PAOTPage);