import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import he from '../localization/he';
import en from '../localization/en';
import Header from '../components/Header';
import Gallery, { PhotoProps } from "react-photo-gallery";
import ILocalization from '../localization/interface';
import Carousel, { Modal, ModalGateway } from "react-images";
import { IPhoto } from '../store/landing/types';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { getGallery } from '../store/landing/actions';
import { connect } from 'react-redux';


interface props extends RouteComponentProps {
    getGallery: () => Promise<void>;
    gallery: IPhoto[];
}

interface state {
    viewerIsOpen: boolean;
    currentImageInd: number;
}

class GalleryPage extends Component<props, state> {
    state = {
        viewerIsOpen: false,
        currentImageInd: 0
    }

    componentDidMount() {
        this.props.getGallery();
    }

    openLightbox = (
        event: React.MouseEvent, 
        photos: {
            index: number
            next: PhotoProps | null
            photo: PhotoProps
            previous: PhotoProps | null
        }
    ) => {
        this.setState({
            viewerIsOpen: true,
            currentImageInd: photos.index
        });
    }

    closeLightbox = () => {
        this.setState({
            viewerIsOpen: false,
            currentImageInd: 0
        });
    }

    render() {
        const lang:any = this.props.match.params;
        const locationObj:ILocalization = lang.lang === "he" ? he : en;
        const photos:IPhoto[] = this.props.gallery;
        
        
        return (
            <div className={`${lang.lang === "he" ? "he" : "en"}`}>
                <Header />
                <div className="gallery">
                    <Gallery photos={photos.map((photo) => ({...photo, src: photo.preview}))} onClick={this.openLightbox} />
                    <ModalGateway>
                        {this.state.viewerIsOpen ? (
                        <Modal 
                            onClose={this.closeLightbox}
                            allowFullscreen={false}
                            styles={{
                                blanket: base => ({
                                  ...base,
                                }),
                                positioner: base => ({
                                  ...base,
                                  display: 'block',
                                }),
                              }}
                        >
                            <Carousel
                                currentIndex={this.state.currentImageInd}
                                views={photos.map(x => ({
                                    ...x,
                                    source: x.src,
                                    srcset: x.srcSet,
                                    caption: x.title
                                }))}
                                styles={{
                                    container: base => ({
                                      ...base,
                                      height: '100vh',
                                      alignItems: 'center',
                                      justifyContent: 'center',
                                      display: "flex"
                                    }),
                                    view: base => ({
                                      ...base,
                                      alignItems: 'center',
                                      display: 'flex ',
                                      height: 'calc(100vh - 54px)',
                                      width: '80%',
                                      margin: "0 auto",
                                      justifyContent: 'center',
                  
                                      '& > img': {
                                        maxHeight: 'calc(90vh - 94px)',
                                      },
                                    }),
                                    footer: (base, state) => {                                  
                                      return { ...base, fontSize: "24px" };
                                    }
                                  }}
                            />
                        </Modal>
                        ) : null}
                    </ModalGateway>
                </div>
				<footer>
                    <div className="ss-links">
                        <a className="instagram" href="https://www.instagram.com/blue_mountains_tours__/"></a>
                        <a className="linkedin" href="https://www.linkedin.com/in/menashe-salomon-a262015b/?challengeId=AQG-W2BBTc6jiAAAAXMfIBgsanfx3DE5t5e8TtcyK7wcBz_2pKs1zEUlP60ysC28I0YDKkopRbTbRwqxFYv0HSH3VrlBqw-quA&submissionId=0b2d1266-afdd-1e16-a263-3583750c4d33"></a>
                        <a className="facebook" href="https://www.facebook.com/zanzatours/"></a>
                    </div>
					© 2020 zanzatours.com
				</footer>
            </div>
        )
    }
}
const navButtonStyles = (base:any) => ({
    ...base,
    backgroundColor: 'white',
    boxShadow: '0 1px 6px rgba(0, 0, 0, 0.18)',
  
    '&:hover, &:active': {
      backgroundColor: 'white',
      opacity: 1,
    },
    '&:active': {
      boxShadow: '0 1px 3px rgba(0, 0, 0, 0.14)',
      transform: 'scale(0.96)',
    },
  });


const mapStateToProps = (state: AppState) => ({
    gallery: state.landing.gallery
});


const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getGallery: async () => {
        dispatch(getGallery());
    }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GalleryPage));