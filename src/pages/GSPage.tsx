import React, { Component } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import MainBlockForToursPage from '../components/MainBlockForToursPage';
import Header from '../components/Header';
import { ITourPropertyCard } from '../components/TourPropertyCard';
import TourPropertyCardContainer from '../components/containers/TourPropertyCardContainer';
import ContactUs from '../components/ContactUs';
import ToursPageFooter from '../components/ToursPageFooter';

interface props extends RouteComponentProps {

}

class GSPage extends Component<props, {}> {
    render() {
		const lang:any = this.props.match.params;
        return (
            <div className={`tours-page ${lang.lang === "he" ? "he" : "en"}`}>
                <Header />
                <MainBlockForToursPage
                    title="ground services"
                    description={`We can take care of all your surrounding 
                    services around your vacation/tours to become a more 
                    relaxing and stress-free time. Also, with our expertise 
                    get the best deals and work with previously checked 
                    companies.`}
                />
                <div className="content gs-page">
                    <TourPropertyCardContainer
                        properties={properties}
                    />
                    <div className="line first"></div>
                    <div className="line second"></div>
                    <div className="line third"></div>
                    <div className="shape first"></div>
                    <div className="shape second"></div>
                </div>
                <ContactUs />
                <ToursPageFooter />
            </div>
        )
    }
}

const properties:ITourPropertyCard[] = [
    {title: "Flights", icon_name: "flights", color: "main_blue", description: "Comfortable and convenient schedule"},
    {title: "Car rentals", icon_name: "car_rentals", color: "dark_blue", description: "Get the best deals with our experience"},
    {title: "Insurances", icon_name: "insurances", color: "red", description: "Cover everything you will need and nothing more"},
    {title: "Hotels", icon_name: "hotels", color: "yellow", description: "Best locations and service"},
    {title: "Attractions", icon_name: "attractions", color: "dark_blue", description: "Variety of activities to fit anyone"},
    {title: "Tour guides", icon_name: "tour_guides", color: "pink", description: "In any language and to fit any tour"}
]


export default withRouter(GSPage);