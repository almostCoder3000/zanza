import React, { Component } from 'react';
import ILocalization from '../localization/interface';
import Header from '../components/Header';
import he from '../localization/he';
import en from '../localization/en';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Paginator from '../components/Paginator';
import TourCard from '../components/TourCard';
import { ITour } from '../store/landing/types';
import { AppState } from '../store';
import { ThunkDispatch } from 'redux-thunk';
import { getTours } from '../store/landing/actions';
import { connect } from 'react-redux';

interface props extends RouteComponentProps {
	tours: ITour[];
	getTours: () => Promise<void>;
}

interface state {
	currentTourCardInd: number;
}

class MPTNewZealand extends Component<props, state> {
	state = {
		currentTourCardInd: 0
    }

	componentDidMount() {
		this.props.getTours();
	}
    
	_renderTours() {
		const lang:any = this.props.match.params;
		return this.props.tours.map((tour:ITour, ind:number) => {
			return (
				<TourCard
					key={ind}
					title_hebrew={tour.title_hebrew}
					description_hebrew={tour.description_hebrew}
					lang={lang.lang}
					title={tour.title}
					description={tour.description}
					photo={tour.photo}
					price={tour.price}
					on_click_bookatour={tour.on_click_bookatour}
					style={{transform: `translateX(-${320*(this.state.currentTourCardInd)}px)`}}
				/>
			)
		})
    }
    
    _switchTour(indexTour:number) {
		this.setState({currentTourCardInd: indexTour});	
    }
    
    render() {
		const lang:any = this.props.match.params;
		const locationObj:ILocalization = lang.lang === "he" ? he : en;	
        return (
            <div className={`${lang.lang === "he" ? "he" : "en"}`}>
                <Header />
                <div className="tours" id="most_popular">
					<h1>{locationObj.mostPopularPage.nz.title}</h1>
					<div className="divider"></div>
					<div className="desktop-line"></div>
					<div className="cards">
						{this._renderTours.bind(this)()}
					</div>
                    <Paginator 
                        isTourCards={true} 
                        count={this.props.tours.length} 
                        switch={this._switchTour.bind(this)} 
                    />
				</div>
				<footer>
                    <div className="ss-links">
						<a className="instagram" href="https://www.instagram.com/blue_mountains_tours__/"></a>
                        <a className="linkedin" href="https://www.linkedin.com/in/menashe-salomon-a262015b/?challengeId=AQG-W2BBTc6jiAAAAXMfIBgsanfx3DE5t5e8TtcyK7wcBz_2pKs1zEUlP60ysC28I0YDKkopRbTbRwqxFYv0HSH3VrlBqw-quA&submissionId=0b2d1266-afdd-1e16-a263-3583750c4d33"></a>
                        <a className="facebook" href="https://www.facebook.com/zanzatours/"></a>
                    </div>
					© 2020 zanzatours.com
				</footer>
            </div>
        )
    }
}


const mapStateToProps = (state: AppState) => ({
	tours: state.landing.tours
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
	getTours: async () => {
		dispatch(getTours("nz"));
	}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MPTNewZealand));