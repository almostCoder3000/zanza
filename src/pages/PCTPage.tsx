import React, { Component } from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'
import MainBlockForToursPage from '../components/MainBlockForToursPage';
import Header from '../components/Header';
import { ITourPropertyCard } from '../components/TourPropertyCard';
import TourPropertyCardContainer from '../components/containers/TourPropertyCardContainer';
import ContactUs from '../components/ContactUs';
import ToursPageFooter from '../components/ToursPageFooter';

interface props extends RouteComponentProps {

}

class PCTPage extends Component<props, {}> {
    render() {
		const lang:any = this.props.match.params;
        return (
            <div className={`tours-page ${lang.lang === "he" ? "he" : "en"}`}>
                <Header />
                <MainBlockForToursPage
                    title="personal customized tours"
                    description={`Fully customized plan that allows 
                    you to pick the dates, locations, and level of 
                    service for your tour.`}
                />
                <div className="content pct-page">
                    <TourPropertyCardContainer
                        properties={properties}
                    />
                    <div className="line first"></div>
                    <div className="line second"></div>
                    <div className="line third"></div>
                    <div className="shape first"></div>
                    <div className="shape second"></div>
                </div>
                <ContactUs />
                <ToursPageFooter />
            </div>
        )
    }
}

const properties:ITourPropertyCard[] = [
    {title: "Dates", icon_name: "dates", color: "yellow", description: "15.03 – 12.04"},
    {title: "Numbers of travelers", icon_name: "numbers_of_travelers", color: "main_blue", description: "17400"},
    {title: "General Interested Areas", icon_name: "general_interested_areas", color: "green", description: "Australia, New Zealand, and breakdown by region"},
    {title: "Level of personal service", icon_name: "level_of_personal_service", color: "yellow", description: "Close hand-holding service or looser behind the scene work"},
    {title: "Type Of Travel", icon_name: "type_of_travel", color: "green", description: "Honeymoon, family retreat, active, backpacking, luxurious"},
    {title: "Side services", icon_name: "side_services", color: "dark_blue", description: "Flights, car rentals, hotels, concierge"},
    {title: "Anything else", icon_name: "anything_else", color: "main_blue", description: "We can accommodate"},
]


export default withRouter(PCTPage);